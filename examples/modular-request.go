package main

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

const urlTemplate = "http://localhost:8080/user/%s"

type User struct {
	ID string
	Username string
	Email string
}

func GetUserWithUsernameAndPassword(userID string, username, password string) *User {
	return getUser(userID, SetAuthorization(basicAuth(username, password)))
}

func GetUserWithAuthToken(userID string, authToken string) *User {
	return getUser(userID, SetAuthorization(authToken))
}

func getUser(userID string, authorizationSetter func(request *http.Request)) *User {
	request, _ := http.NewRequest("get", fmt.Sprintf(urlTemplate, userID), nil)
	authorizationSetter(request)
	response, _ := (&http.Client{}).Do(request)
	return responseToUser(response)
}

func SetAuthorization(token string) func(request *http.Request) {
	return func(request *http.Request) {
		request.Header.Set("Authorization", fmt.Sprintf("Basic %s", token))
	}
}

func basicAuth(username, password string) string {
	return base64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("%s:%s", username, password)))
}

func responseToUser(response *http.Response) *User {
	body, _ := ioutil.ReadAll(response.Body)
	var user User
	_ = json.Unmarshal(body, &user)
	return &user
}


func main() {

}

"""
structured code

run: PYTHONPATH=examples poetry run uvicorn structured:app --reload

Then go to http://localhost:8000/docs

joaonrb 2021-08-15
"""
import asyncio
import pickle
from abc import ABC, abstractmethod

import aiofiles
from pydantic import BaseModel, EmailStr
from fastapi import FastAPI, Path, HTTPException


class Database:
    _lock = asyncio.Lock()
    _db = "/tmp/structured.db"

    @classmethod
    async def db(cls, no_lock=False):
        try:
            if no_lock:
                return await cls.loads_db()
            async with cls._lock:
                return await cls.loads_db()
        except FileNotFoundError:
            return {}

    @classmethod
    async def get(cls, key):
        return (await cls.db()).get(key)

    @classmethod
    async def set(cls, key, obj):
        async with cls._lock:
            db = await cls.db(no_lock=True)
            db[key] = obj
            await cls.dump_db(db)
        return obj

    @classmethod
    async def dump_db(cls, db):
        async with aiofiles.open(cls._db, mode="wb") as f:
            await f.write(pickle.dumps(db))

    @classmethod
    async def loads_db(cls):
        async with aiofiles.open(cls._db, mode="rb") as f:
            return pickle.loads(await f.read())


class InputModelUser(BaseModel):
    id: int
    username: str
    email: EmailStr

    def to_db(self):
        return DatabaseModelUser(id=self.id, username=self.username, email=self.email)


class OutputModelUser(BaseModel):
    id: int
    username: str
    email: EmailStr

    @classmethod
    def from_db(cls, user):
        return cls(id=user.id, username=user.username, email=user.email)


class DatabaseModelUser(BaseModel):
    id: int
    username: str
    email: EmailStr


class DataController(ABC):
    @abstractmethod
    async def get_profile(self, user_id: int) -> DatabaseModelUser:
        pass

    @abstractmethod
    async def set_profile(self, user: DatabaseModelUser) -> DatabaseModelUser:
        pass


class FakeDataController(DataController):
    async def get_profile(self, user_id: int) -> DatabaseModelUser:
        return await Database.get(user_id)

    async def set_profile(self, user: DatabaseModelUser) -> DatabaseModelUser:
        return await Database.set(user.id, user)


app = FastAPI()


def data_controller():
    return FakeDataController()


@app.get("/user/{userID}")
async def get_profile(user_id: int = Path(..., alias="userID")):
    if user := await data_controller().get_profile(user_id):
        return OutputModelUser.from_db(user)
    raise HTTPException(status_code=404, detail="User not found")


@app.post("/user")
async def post_profile(user: InputModelUser):
    return await data_controller().set_profile(user.to_db())

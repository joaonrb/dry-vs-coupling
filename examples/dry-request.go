package main

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

const urlTemplate = "http://localhost:8080/user/%s"

type User struct {
	ID string
	Username string
	Email string
}

func GetUserWithUsernameAndPassword(userID string, username, password string) *User {
	return getUser(userID, &username, &password, nil)
}

func GetUserWithAuthToken(userID string, authToken string) *User {
	return getUser(userID, nil, nil, &authToken)
}

func getUser(userID string, username, password, authToken *string) *User {
	request, _ := http.NewRequest("get", fmt.Sprintf(urlTemplate, userID), nil)
	switch {
	case username != nil && password != nil:
		setAuthorization(request, basicAuth(*username, *password))
	case authToken != nil:
		setAuthorization(request, *authToken)
	}
	response, _ := (&http.Client{}).Do(request)
	return responseToUser(response)
}

func setAuthorization(request *http.Request, token string) {
	request.Header.Set("Authorization", fmt.Sprintf("Basic %s", token))
}

func basicAuth(username, password string) string {
	return base64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("%s:%s", username, password)))
}

func responseToUser(response *http.Response) *User {
	body, _ := ioutil.ReadAll(response.Body)
	var user User
	_ = json.Unmarshal(body, &user)
	return &user
}


func main() {

}

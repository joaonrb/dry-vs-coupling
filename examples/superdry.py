"""
superdry code

run: PYTHONPATH=examples poetry run uvicorn superdry:app --reload

Then go to http://localhost:8000/docs

joaonrb 2021-08-15
"""
import asyncio
import pickle

import aiofiles
from pydantic import BaseModel, EmailStr
from fastapi import FastAPI, Path, HTTPException


class Database:
    _lock = asyncio.Lock()
    _db = "/tmp/superdry.db"

    @classmethod
    async def db(cls, no_lock=False):
        try:
            if no_lock:
                return await cls.loads_db()
            async with cls._lock:
                return await cls.loads_db()
        except FileNotFoundError:
            return {}

    @classmethod
    async def get(cls, key):
        return (await cls.db()).get(key)

    @classmethod
    async def set(cls, key, obj):
        async with cls._lock:
            db = await cls.db(no_lock=True)
            db[key] = obj
            await cls.dump_db(db)
        return obj

    @classmethod
    async def dump_db(cls, db):
        async with aiofiles.open(cls._db, mode="wb") as f:
            await f.write(pickle.dumps(db))

    @classmethod
    async def loads_db(cls):
        async with aiofiles.open(cls._db, mode="rb") as f:
            return pickle.loads(await f.read())


# The user data model
class User(BaseModel):
    id: int
    username: str
    email: EmailStr


app = FastAPI()


@app.get("/user/{userID}")
async def get_profile(user_id: int = Path(..., alias="userID")):
    if user := await Database.get(user_id):
        return user
    raise HTTPException(status_code=404, detail="User not found")


@app.post("/user")
async def post_profile(user: User):
    return await Database.set(user.id, user)
